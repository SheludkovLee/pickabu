import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { FavoritePostModel } from '../../models/post-activity.model';
import { FavoritePostsService } from '../../services/posts-activity.service';
import { ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';
import { Self } from 'src/base/decorators/self.decorator';
import { PostActivity } from '../../entities/post-activity.entity';

@UseGuards(JwtAuthGuard)
@Resolver(() => FavoritePostModel)
export class FavoritePostsMutationResolver {
  constructor(private favoritePostsService: FavoritePostsService) {}

  @Mutation(() => FavoritePostModel)
  async addPostToFavorite(
    @Self('id') userId: string,
    @Args('postId', ParseUUIDPipe) postId: string,
  ): Promise<FavoritePostModel> {
    const entity: PostActivity = await this.favoritePostsService.addToFavorite({
      userId,
      postId,
    });
    return FavoritePostModel.create(entity);
  }

  @Mutation(() => Boolean)
  removePostFromFavorite(
    @Self('id') userId: string,
    @Args('postId', ParseUUIDPipe) postId: string,
  ): Promise<boolean> {
    return this.favoritePostsService.removeFromFavorite({
      userId,
      postId,
    });
  }
}
