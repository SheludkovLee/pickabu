import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { RatingCommentModel } from '../../models/comment-activity.model';
import { RatingCommentsService } from '../../services/comments-activity.service';
import { ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';
import { Self } from 'src/base/decorators/self.decorator';
import { SetRateInput } from '../../inputs/set-rate.input';
import { CommentActivity } from '../../entities/comment-activity.entity';

@UseGuards(JwtAuthGuard)
@Resolver(() => RatingCommentModel)
export class RatingCommentsMutationResolver {
  constructor(private ratingCommentsService: RatingCommentsService) {}

  @Mutation(() => RatingCommentModel)
  async setRateToComment(
    @Self('id') userId: string,
    @Args('commentId', ParseUUIDPipe) commentId: string,
    @Args('setRateInput') input: SetRateInput,
  ): Promise<RatingCommentModel> {
    const entity: CommentActivity = await this.ratingCommentsService.setRate({
      userId,
      commentId,
      ...input,
    });
    return RatingCommentModel.create(entity);
  }

  @Mutation(() => Boolean)
  removeCommentRate(
    @Self('id') userId: string,
    @Args('commentId', ParseUUIDPipe) commentId: string,
  ): Promise<boolean> {
    return this.ratingCommentsService.removeRate({
      userId,
      commentId,
    });
  }
}
