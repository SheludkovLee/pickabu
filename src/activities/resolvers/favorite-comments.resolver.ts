import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { FavoriteCommentModel } from '../models/comment-activity.model';
import { CommentModel } from 'src/comments/models/comment.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from 'src/comments/entities/comment.entity';
import { Repository } from 'typeorm';
import { Loader } from '@purrweb/dataloader';
import { CommentLoader } from 'src/comments/dataloaders/comment.loader';
import DataLoader from 'dataloader';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Resolver(() => FavoriteCommentModel)
export class FavoriteCommentsResolver {
  constructor(
    @InjectRepository(Comment)
    private readonly commentsRepository: Repository<Comment>,
  ) {}

  @ResolveField('comment', () => CommentModel)
  comment(
    @Parent() favorite: FavoriteCommentModel,
    @Loader(CommentLoader) commentLoader: DataLoader<string, CommentModel>,
  ): Promise<CommentModel> {
    const { commentId } = favorite;
    return commentLoader.load(commentId);
  }
}
