import { Injectable } from '@nestjs/common';
import { Activity } from '../entities/activity.entity';
import { ActivityRepository } from '../repositories/activity.repository';
import { DeepPartial, FindConditions, FindOneOptions } from 'typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';

@Injectable()
export class FavoriteActivityService<T extends Activity> {
  protected constructor(private repo: ActivityRepository<T>) {}

  async addToFavorite(dto: DeepPartial<T>): Promise<T> {
    return this.repo.updateOrInsert(
      { ...dto, isFavorite: true },
      { where: { ...dto } },
    );
  }

  async removeFromFavorite(conditions: FindConditions<T>): Promise<boolean> {
    const favorite = await this.repo.findOneOrFail(conditions, {
      where: { isFavorite: 1 },
    });
    await this.repo.save({ ...favorite, isFavorite: 0 });

    if (favorite.isLike) {
      await this.repo.remove(favorite);
    }
    return true;
  }

  async paginate(
    options: IPaginationOptions,
    searchOptions?: FindConditions<T>,
  ): Promise<Pagination<T>> {
    const findOptions: FindOneOptions<T> = {
      where: { ...searchOptions, isFavorite: 1 },
    };
    return paginate<T>(this.repo, options, findOptions);
  }
}
