import { Field, InputType } from '@nestjs/graphql';
import { IsBoolean } from 'class-validator';

@InputType()
export class SetRateInput {
  @Field()
  @IsBoolean()
  isLike!: boolean;
}
