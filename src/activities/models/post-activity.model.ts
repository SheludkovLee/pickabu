import { RatingModel } from './rating.model';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { FavoriteModel } from './favorite.model';
import { PostActivity } from '../entities/post-activity.entity';
import { plainToInstance } from 'class-transformer';

@ObjectType('FavoritePost')
export class FavoritePostModel extends FavoriteModel {
  @Field(() => ID)
  postId!: string;

  static create(entity: PostActivity) {
    return plainToInstance(this, entity);
  }
}

@ObjectType('RatingPost')
export class RatingPostModel extends RatingModel {
  @Field(() => ID)
  postId!: string;

  static create(entity: PostActivity) {
    return plainToInstance(this, entity);
  }
}
