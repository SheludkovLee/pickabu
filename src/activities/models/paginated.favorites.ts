import { Field, ObjectType } from '@nestjs/graphql';
import { PaginatedModel } from 'src/base/models/paginated.model';
import { FavoriteCommentModel } from './comment-activity.model';
import { FavoritePostModel } from './post-activity.model';
import { Pagination } from 'nestjs-typeorm-paginate';
import { PostActivity } from '../entities/post-activity.entity';
import { plainToInstance } from 'class-transformer';
import { CommentActivity } from '../entities/comment-activity.entity';

@ObjectType('PaginatedFavoriteComments')
export class PaginatedFavoriteCommentsModel extends PaginatedModel {
  @Field(() => [FavoriteCommentModel])
  items!: FavoriteCommentModel[];

  static create(pagination: Pagination<CommentActivity>) {
    const items = pagination.items.map(FavoriteCommentModel.create);
    return plainToInstance(this, { items: items, meta: pagination.meta });
  }
}

@ObjectType('PaginatedFavoritePosts')
export class PaginatedFavoritePostsModel extends PaginatedModel {
  @Field(() => [FavoritePostModel])
  items!: FavoritePostModel[];

  static create(pagination: Pagination<PostActivity>) {
    const items = pagination.items.map(FavoritePostModel.create);
    return plainToInstance(this, { items: items, meta: pagination.meta });
  }
}
