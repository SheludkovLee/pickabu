import { ActivityRepository } from './activity.repository';
import { EntityRepository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { CommentActivity } from '../entities/comment-activity.entity';

@Injectable()
@EntityRepository(CommentActivity)
export class CommentActivityRepository extends ActivityRepository<CommentActivity> {}
