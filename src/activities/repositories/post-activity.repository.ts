import { ActivityRepository } from './activity.repository';
import { PostActivity } from '../entities/post-activity.entity';
import { EntityRepository } from 'typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
@EntityRepository(PostActivity)
export class PostActivityRepository extends ActivityRepository<PostActivity> {}
