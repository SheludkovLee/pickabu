import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Activity } from './entities/activity.entity';
import { PostActivity } from './entities/post-activity.entity';
import { CommentActivity } from './entities/comment-activity.entity';
import { PostActivityRepository } from './repositories/post-activity.repository';
import { ActivityRepository } from './repositories/activity.repository';
import { FavoritePostsResolver } from './resolvers/favorite-posts.resolver';
import {
  FavoritePostsService,
  RatingPostsService,
} from './services/posts-activity.service';
import { FavoriteCommentsResolver } from './resolvers/favorite-comments.resolver';
import {
  FavoriteCommentsService,
  RatingCommentsService,
} from './services/comments-activity.service';
import { CommentActivityRepository } from './repositories/comment-activity.repository';
import { FavoritePostsMutationResolver } from './resolvers/mutation/favorite-posts.mutation.resolver';
import { FavoriteCommentsMutationResolver } from './resolvers/mutation/favorite-comments-mutation.resolver';
import { RatingPostsMutationResolver } from './resolvers/mutation/rating-posts-mutation.resolver';
import { RatingCommentsMutationResolver } from './resolvers/mutation/rating-comments-mutation.resolver';
import { Post } from 'src/posts/entities/post.entity';
import { Comment } from 'src/comments/entities/comment.entity';
import { PostLoader } from '../posts/dataloaders/post.loader';
import { CommentLoader } from '../comments/dataloaders/comment.loader';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Activity,
      ActivityRepository,
      PostActivityRepository,
      CommentActivityRepository,
      PostActivity,
      CommentActivity,
      Post,
      Comment,
      PostLoader,
      CommentLoader,
    ]),
  ],
  providers: [
    FavoritePostsResolver,
    FavoriteCommentsResolver,
    FavoritePostsMutationResolver,
    FavoriteCommentsMutationResolver,
    RatingPostsMutationResolver,
    RatingCommentsMutationResolver,
    FavoritePostsService,
    RatingPostsService,
    FavoriteCommentsService,
    RatingCommentsService,
  ],
  exports: [FavoriteCommentsService, FavoritePostsService],
})
export class ActivitiesModule {}
