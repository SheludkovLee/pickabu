import { IsOwnerGuard } from 'src/base/guards/is-owner.guard';
import { Post } from '../entities/post.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class IsOwnerPostGuard extends IsOwnerGuard<Post> {
  constructor() {
    super(Post);
  }
}
