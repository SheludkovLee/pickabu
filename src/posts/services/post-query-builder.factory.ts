import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from '../entities/post.entity';
import { PostsLikesViewEntity } from '../views/posts-likes-view.entity';
import { PostsCommentsViewEntity } from '../views/posts-comments-view.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';

@Injectable()
export class PostQueryBuilderFactory {
  constructor(
    @InjectRepository(Post) private postRepo: Repository<Post>,
    @InjectRepository(PostsLikesViewEntity)
    private likesViewRepo: Repository<PostsLikesViewEntity>,
    @InjectRepository(PostsCommentsViewEntity)
    private commentsViewRepo: Repository<PostsCommentsViewEntity>,
  ) {}

  create(
    sort: string,
  ): SelectQueryBuilder<Post | PostsLikesViewEntity | PostsCommentsViewEntity> {
    switch (sort) {
      case 'likes':
        return this.likesViewRepo.createQueryBuilder('likes');
      case 'comments':
        return this.commentsViewRepo.createQueryBuilder('comments');
      default:
        return this.postRepo.createQueryBuilder('post');
    }
  }
}
