import { PostView } from './post-view.entity';
import { Connection, ViewColumn, ViewEntity } from 'typeorm';
import { Post } from '../entities/post.entity';

@ViewEntity({
  expression: (connection: Connection) =>
    connection
      .createQueryBuilder(Post, 'post')
      .select('post.*')
      .leftJoin('post.activities', 'activity')
      .addSelect('COUNT(activity.id)', 'likes')
      .where('activity.isLike = true')
      .groupBy('post.id'),
})
export class PostsLikesViewEntity extends PostView {
  @ViewColumn()
  likes!: number;
}
