import { PostView } from './post-view.entity';
import { Connection, ViewColumn, ViewEntity } from 'typeorm';
import { Post } from '../entities/post.entity';

@ViewEntity({
  expression: (connection: Connection) =>
    connection
      .createQueryBuilder(Post, 'post')
      .select('post.*')
      .leftJoin('post.comments', 'comment')
      .addSelect('COUNT(comment.id)', 'comments')
      .groupBy('post.id'),
})
export class PostsCommentsViewEntity extends PostView {
  @ViewColumn()
  comments!: number;
}
