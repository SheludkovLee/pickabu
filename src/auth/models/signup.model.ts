import { Field, ObjectType } from '@nestjs/graphql';
import { UserModel } from 'src/users/models/user.model';
import { LoginModel } from './login.model';

@ObjectType('Signup')
export class SignupModel extends LoginModel {
  @Field(() => UserModel)
  user!: UserModel;
}
