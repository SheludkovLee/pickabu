import { BadRequestException, Injectable } from '@nestjs/common';
import { User } from 'src/users/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { SignupUserInput } from './inputs/signup-user.input';
import { HashService } from 'src/users/services/hash.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersService } from 'src/users/services/users.service';
import { LoginModel } from './models/login.model';
import { SignupModel } from './models/signup.model';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private usersService: UsersService,
    private hashService: HashService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<User | null> {
    const user = await this.usersRepository.findOne({
      where: { email: email },
    });
    if (!user) {
      return null;
    }

    const isRealPassword: boolean = await this.hashService.compareHash(
      password,
      user.password,
    );

    return isRealPassword ? user : null;
  }

  async login(user: User): Promise<LoginModel> {
    const payload = { email: user.email, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async signup(input: SignupUserInput): Promise<SignupModel> {
    const candidate = await this.usersRepository.findOne({
      where: { email: input.email },
    });

    if (candidate) {
      throw new BadRequestException(
        `User with email: ${candidate.email} already exists`,
      );
    }

    const newUser = await this.usersService.create(input);
    const { access_token } = await this.login(newUser);

    return {
      access_token,
      user: newUser,
    };
  }
}
