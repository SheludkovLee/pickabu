import { Args, Context, Mutation, Resolver } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { LoginModel } from './models/login.model';
import { LoginUserInput } from './inputs/login-user.input';
import { UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from './strategies/local/local-auth.guard';
import { User } from 'src/users/entities/user.entity';
import { SignupModel } from './models/signup.model';
import { SignupUserInput } from './inputs/signup-user.input';

@Resolver()
export class AuthResolver {
  constructor(private authService: AuthService) {}

  @Mutation(() => LoginModel)
  @UseGuards(LocalAuthGuard)
  login(
    @Args('loginUserInput') input: LoginUserInput,
    @Context() context: { user: User },
  ): Promise<LoginModel> {
    return this.authService.login(context.user);
  }

  @Mutation(() => SignupModel)
  signup(
    @Args('signupUserInput') input: SignupUserInput,
  ): Promise<SignupModel> {
    return this.authService.signup(input);
  }
}
