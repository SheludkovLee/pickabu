import { ArgsType, Field } from '@nestjs/graphql';
import { IsIn, IsOptional } from 'class-validator';

@ArgsType()
export class SortArgs {
  @Field({ defaultValue: 'created_at' })
  @IsOptional()
  @IsIn(['created_at', 'likes'])
  sort!: string;

  @Field(() => String, { defaultValue: 'DESC' })
  @IsOptional()
  @IsIn(['ASC', 'DESC'])
  order!: 'ASC' | 'DESC';
}
