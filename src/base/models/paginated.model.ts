import { Field, ObjectType } from '@nestjs/graphql';
import { PaginationMeta } from './pagination-meta.model';

@ObjectType({ isAbstract: true })
export class PaginatedModel {
  @Field(() => PaginationMeta)
  meta!: PaginationMeta;
}
