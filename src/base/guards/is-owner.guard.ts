import { CanActivate, ExecutionContext } from '@nestjs/common';
import { EntityTarget, getRepository, Repository } from 'typeorm';
import { GqlExecutionContext } from '@nestjs/graphql';

export abstract class IsOwnerGuard<Entity extends { userId: string }>
  implements CanActivate
{
  protected constructor(
    private entity: EntityTarget<Entity>,
    private idParam: string = 'id',
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();
    const { user, body } = req;

    if (!user || !body) {
      return false;
    }

    const authUserId: string = user.id;
    const idItem = this.getItemId(req);

    const repo: Repository<Entity> = getRepository(this.entity);

    const item: Entity | undefined = await repo.findOne(idItem);

    if (!item) {
      return true;
    }

    return item.userId === authUserId;
  }

  protected getItemId(req: any): number {
    const { body } = req;
    return body.variables[this.idParam];
  }
}
