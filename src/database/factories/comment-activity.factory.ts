import { define } from 'typeorm-seeding';
import { Faker } from '@faker-js/faker';
import { CommentActivity } from 'src/activities/entities/comment-activity.entity';

define(CommentActivity, (faker: Faker) => {
  const commentActivity = new CommentActivity();

  commentActivity.isFavorite = faker.datatype.boolean();
  commentActivity.isLike = faker.datatype.boolean();

  return commentActivity;
});
