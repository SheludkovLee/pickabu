import { define } from 'typeorm-seeding';
import { Faker } from '@faker-js/faker';
import { Post } from 'src/posts/entities/post.entity';

define(Post, (faker: Faker) => {
  const post = new Post();

  const urls = [...Array(5)].map(() => faker.internet.url());
  const tags = [...Array(5)].map(() => faker.lorem.word());

  post.title = faker.lorem.slug();
  post.text = faker.lorem.text();
  post.images = urls;
  post.tags = tags;

  return post;
});
