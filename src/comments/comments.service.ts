import { Injectable } from '@nestjs/common';
import { CreateCommentInput } from './inputs/create-comment.input';
import { UpdateCommentInput } from './inputs/update-comment.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from './entities/comment.entity';
import { Repository } from 'typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { SortArgs } from '../base/args/sort.args';

@Injectable()
export class CommentsService {
  constructor(@InjectRepository(Comment) private repo: Repository<Comment>) {}

  async create(userId: string, input: CreateCommentInput): Promise<Comment> {
    return this.repo.save({ userId, ...input });
  }

  async paginate(
    options: IPaginationOptions,
    { sort, order }: SortArgs,
    postId: string,
  ): Promise<Pagination<Comment>> {
    const queryBuilder = this.repo.createQueryBuilder();
    queryBuilder.orderBy(sort, order).where(postId);

    return paginate<Comment>(queryBuilder, options);
  }

  async findOne(id: string): Promise<Comment> {
    return this.repo.findOneOrFail(id);
  }

  async update(id: string, input: UpdateCommentInput): Promise<Comment> {
    await this.repo.update(id, input);
    return this.findOne(id);
  }

  async remove(id: string): Promise<boolean> {
    const comment: Comment = await this.findOne(id);
    await this.repo.remove(comment);
    return true;
  }
}
