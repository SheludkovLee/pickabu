import { IsOwnerGuard } from 'src/base/guards/is-owner.guard';
import { Injectable } from '@nestjs/common';
import { Comment } from '../entities/comment.entity';

@Injectable()
export class IsOwnerCommentGuard extends IsOwnerGuard<Comment> {
  constructor() {
    super(Comment);
  }
}
