import { Field, ObjectType } from '@nestjs/graphql';
import { CommentModel } from './comment.model';
import { PaginatedModel } from 'src/base/models/paginated.model';
import { Pagination } from 'nestjs-typeorm-paginate';
import { plainToInstance } from 'class-transformer';
import { Comment } from '../entities/comment.entity';

@ObjectType('PaginatedComments')
export class PaginatedCommentsModel extends PaginatedModel {
  @Field(() => [CommentModel])
  items!: CommentModel[];

  static create(pagination: Pagination<Comment>) {
    const items = pagination.items.map(CommentModel.create);
    return plainToInstance(this, { items: items, meta: pagination.meta });
  }
}
