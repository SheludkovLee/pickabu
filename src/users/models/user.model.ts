import { BaseModel } from 'src/base/models/base.model';
import { Field, ObjectType } from '@nestjs/graphql';
import { User } from '../entities/user.entity';
import { plainToInstance } from 'class-transformer';

@ObjectType('User')
export class UserModel extends BaseModel {
  @Field()
  email!: string;

  static create(entity: User): UserModel {
    //TODO
    return plainToInstance(this, entity);
  }
}
