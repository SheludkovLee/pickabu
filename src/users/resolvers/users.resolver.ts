import { Args, Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { UserModel } from '../models/user.model';
import { FavoriteCommentModel } from 'src/activities/models/comment-activity.model';
import { PaginationArgs } from 'src/base/args/pagination.args';
import {
  PaginatedFavoriteCommentsModel,
  PaginatedFavoritePostsModel,
} from 'src/activities/models/paginated.favorites';
import { FavoritePostModel } from 'src/activities/models/post-activity.model';
import { FavoritePostsService } from 'src/activities/services/posts-activity.service';
import { FavoriteCommentsService } from 'src/activities/services/comments-activity.service';
import { Pagination } from 'nestjs-typeorm-paginate';
import { PostActivity } from '../../activities/entities/post-activity.entity';
import { CommentActivity } from '../../activities/entities/comment-activity.entity';

@Resolver(() => UserModel)
export class UsersResolver {
  constructor(
    private readonly favoritePostsService: FavoritePostsService,
    private readonly favoriteCommentsService: FavoriteCommentsService,
  ) {}

  @ResolveField(() => [FavoritePostModel], { name: 'favoritePosts' })
  async favoritePosts(
    @Args() paginationArgs: PaginationArgs,
    @Parent() user: UserModel,
  ): Promise<PaginatedFavoritePostsModel> {
    const userId = user.id;
    const entities: Pagination<PostActivity> =
      await this.favoritePostsService.paginate(paginationArgs, {
        userId,
      });
    return PaginatedFavoritePostsModel.create(entities);
  }

  @ResolveField(() => [FavoriteCommentModel], { name: 'favoriteComments' })
  async favoriteComments(
    @Args() paginationArgs: PaginationArgs,
    @Parent() user: UserModel,
  ): Promise<PaginatedFavoriteCommentsModel> {
    const userId = user.id;
    const entities: Pagination<CommentActivity> =
      await this.favoriteCommentsService.paginate(paginationArgs, {
        userId,
      });
    return PaginatedFavoriteCommentsModel.create(entities);
  }
}
