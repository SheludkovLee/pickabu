import { Module } from '@nestjs/common';
import { UsersService } from './services/users.service';
import { UsersMutationResolver } from './resolvers/mutation/users-mutation.resolver';
import { UsersResolver } from './resolvers/users.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { HashService } from './services/hash.service';
import { UsersQueryResolver } from './resolvers/query/users-query.resolver';
import { ActivitiesModule } from 'src/activities/activities.module';

@Module({
  imports: [TypeOrmModule.forFeature([User]), ActivitiesModule],
  providers: [
    UsersMutationResolver,
    UsersQueryResolver,
    UsersResolver,
    UsersService,
    HashService,
  ],
  exports: [UsersService, HashService],
})
export class UsersModule {}
